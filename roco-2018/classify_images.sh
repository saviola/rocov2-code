#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

source $DIR/.env

CUDA_VISIBLE_DEVICES=${1:-"0"}

docker run \
    -it \
    --rm \
    --runtime="nvidia" \
    --gpus="$CUDA_VISIBLE_DEVICES"
    -v=/etc/passwd:/etc/passwd:ro \
    -v=/etc/group:/etc/group:ro \
    -v=$DIR/models:/models:ro \
    -v=/path/to/input/images/:/data \
    -v=/path/to/output/:/output \
    -v=$DIR/docker/code/:/code \
    --env DATA_DIR="/data/" \
    --env MODEL_DIR="/models/" \
    --env OUTPUT_DIR="/output/" \
    --env SCRIPT_DIR="/code" \
    --entrypoint="" \
    -P \
    --workdir=/code \
    --user=$(id -u) \
    koitka/roco-classifier:latest -c "/code/infer_figures.sh"
