import argparse
import glob
import numpy as np
import os
import progressbar
import tensorflow as tf


def scan_for_images(input_dir):
    return glob.glob(os.path.join(input_dir, '**', '*.jpg'), recursive=True)


def scan_for_subfigures(input_dir):
    return glob.glob(os.path.join(input_dir, '**', '*-subfigure[0-9]*.jpg'), recursive=True)


def create_input_pipeline(filelist, batch_size=32, parallel_preprocessors=32):
    def preprocess_file(input):
        # Load image from file and decode
        content = tf.read_file(input['path'])
        image = tf.image.decode_image(content, channels=3)
        image = tf.cond(tf.equal(tf.shape(image)[2], 1), lambda: tf.image.grayscale_to_rgb(image), lambda: image)
        image.set_shape([None, None, 3])

        # Apply image preprocessing
        image = tf.image.convert_image_dtype(image, tf.float32)

        shape = tf.to_float(tf.shape(image))
        max_side = tf.maximum(shape[0], shape[1])
        scale_factor = tf.div(299.0, max_side)
        image = tf.image.resize_images(image, tf.to_int32(tf.round((shape[0] * scale_factor, shape[1] * scale_factor))))

        image = tf.image.resize_image_with_crop_or_pad(image, 299, 299)

        # Build final batch dict
        return {
            'image': image,
            'path': input['path'],
        }

    # Create a prefetching and batching data loader
    dataset = tf.data.Dataset.from_tensor_slices({'path': filelist})
    dataset = dataset.map(preprocess_file, num_parallel_calls=parallel_preprocessors)  # Load and preprocess in parallel
    dataset = dataset.batch(batch_size)  # Make a batch of images
    dataset = dataset.prefetch(5)        # Prefetch five batches

    # Create a single iteration iterator
    iterator = dataset.make_one_shot_iterator()
    return iterator.get_next()


def load_model(path, scope):
    with tf.gfile.GFile(path, 'rb') as f:
        graph_def = tf.GraphDef()
        graph_def.ParseFromString(f.read())

    tf.import_graph_def(graph_def, name=scope)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--model_dir', default='/model')
    parser.add_argument('--input_dir', default='/data')
    parser.add_argument('--output_file', default='inference.csv')
    parser.add_argument('--subfigures', action='store_const',
                        const=True, default=False)
    args = parser.parse_args()

    print('Collecting file list...')
    filelist = scan_for_subfigures(args.input_dir) if args.subfigures else scan_for_images(args.input_dir)

    if os.path.exists(args.output_file):
        with open(args.output_file, 'r') as ifile:
            lines = ifile.read().splitlines()
            if len(filelist) == len(lines) - 1:  # CSV output file also contains a header line
                print('Chunk has already been processed! Done!')
                exit()

    print('Loading graphs...')
    with tf.Graph().as_default() as graph:
        data = create_input_pipeline(filelist)
        load_model(os.path.join(args.model_dir, 'compound-figure-detection/frozen_inference_graph.pb'), 'cfd')
        load_model(os.path.join(args.model_dir, 'radiological-figure-detection/frozen_inference_graph.pb'), 'rfd')

        cfd_input = graph.get_tensor_by_name('cfd/input_tensor:0')
        cfd_output = graph.get_tensor_by_name('cfd/output_scores:0')

        rfd_input = graph.get_tensor_by_name('rfd/input_tensor:0')
        rfd_output = graph.get_tensor_by_name('rfd/output_scores:0')

    print('Starting inference...')
    with open(args.output_file, 'w') as ofile:
        ofile.write('path,cfd,cfd-score,rfd,rfd-score\n')

        num_processed = 0
        bar = progressbar.ProgressBar(max_value=len(filelist))
        with tf.Session(graph=graph, config=tf.ConfigProto(gpu_options=tf.GPUOptions(allow_growth=True))) as sess:
            while True:
                try:
                    # Process a batch of images
                    batch_data = sess.run(data)
                    num_elements = len(batch_data['image'])
                    cfd_pred = sess.run(cfd_output, feed_dict={cfd_input: batch_data['image']})
                    cfd_pred_class = np.argmax(cfd_pred, axis=-1)
                    rfd_pred = sess.run(rfd_output, feed_dict={rfd_input: batch_data['image']})  # Potentially restrict to only non-compound figures
                    rfd_pred_class = np.argmax(rfd_pred, axis=-1)

                    # Write results to file
                    for i in range(num_elements):
                        cfd_class = 'NOCOMP' if cfd_pred_class[i] == 0 else 'COMP'
                        cfd_score = cfd_pred[i, cfd_pred_class[i]]
                        rfd_class = 'RAD' if rfd_pred_class[i] == 1 else 'NORAD'
                        rfd_score = rfd_pred[i, rfd_pred_class[i]]

                        ofile.write(','.join([
                            '"' + batch_data['path'][i].decode('ascii') + '"', # quote path
                            cfd_class,
                            str(cfd_score),
                            rfd_class if cfd_class == 'NOCOMP' else '',
                            str(rfd_score) if cfd_class == 'NOCOMP' else '',
                        ]))
                        ofile.write('\n')

                    # Update the progressbar
                    num_processed += num_elements
                    bar.update(num_processed)
                except tf.errors.OutOfRangeError:
                    bar.finish()
                    break
                except ValueError:
                    bar.finish()
                    break
                except:
                    print("Batch error, skipping")

    print('Inference finished!')
