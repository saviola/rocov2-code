#!/usr/bin/env bash

#mkdir -p output

#for CHUNK in {0..255}
for CHUNK in 241
do
    HEX_CHUNK=$(printf "%02x" ${CHUNK})
    echo "----------------------------------------"
    echo "Starting inference on chunk ${HEX_CHUNK}"
    echo "----------------------------------------"
    python3 /code/classify.py \
        --model_dir="/models/" \
        --input_dir="${DATA_DIR}/${HEX_CHUNK}" \
        --output_file="${OUTPUT_DIR}/chunk-${HEX_CHUNK}.csv"
done
