#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

docker build -t koitka/roco-classifier -f $DIR/docker/Dockerfile $DIR/docker
