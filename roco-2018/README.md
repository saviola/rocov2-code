# ROCO compound and radiological figure detection

This folder contains the models and scripts for the original ROCO compound figure and radiological figure detection.

## Requirements

You need to have docker and nvidia-docker installed. The scripts assume a Linux environment.

## Usage

First, build the docker image using `./build_docker_image.sh`.

Then adjust the paths in `./classify_images.sh` to point to the correct input and output directories.

Finally, run `./classify_images.sh`.

## Troubleshooting

If you have issues running the pipeline, [let us know](https://gitlab.com/saviola/rocov2-code/-/issues)!
