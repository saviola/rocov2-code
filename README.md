# ROCOv2 Code

This repository contains the code for the [ROCOv2 dataset](http://doi.org/10.5281/zenodo.10821435). In the subdirectories you will find

- **baseline:** Scripts for training baseline models for both the concept detection and caption prediction subtasks of the ImageCLEFmedical Caption challenge
- **ImageCLEF**: Evaluation scripts for the ImageCLEFmedical Caption challenge tasks
- **roco-2018**: Models and scripts for the original compound figure and radiological figure detection pipeline of the ROCO dataset
