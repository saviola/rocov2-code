# Image Captioning (vision-encoder-text-decoder model) training 

The following example showcases how to finetune a vision-encoder-text-decoder model for image captioning using the PyTorch backend, leveraging 🤗 Transformers library's [VisionEncoderDecoderModel](https://huggingface.co/docs/transformers/main/model_doc/vision-encoder-decoder#initialising-visionencoderdecodermodel-from-a-pretrained-encoder-and-a-pretrained-decoder). This code is adapted from the original Flax example available at [Hugging Face's GitHub repository](https://github.com/huggingface/transformers/tree/main/examples/flax/image-captioning).


To run the code the following steps are required:

# Create a model from a vision encoder model and a text decoder model

First we create a [VisionEncoderDecoderModel](https://huggingface.co/docs/transformers/main/model_doc/vision-encoder-decoder#initialising-visionencoderdecodermodel-from-a-pretrained-encoder-and-a-pretrained-decoder) instance from a pre-trained vision encoder [ViT](https://huggingface.co/docs/transformers/model_doc/vit#transformers.ViTModel) and a pre-trained text decoder [BioMEDLM](https://huggingface.co/stanford-crfm/BioMedLM).

```bash
python3 create_model_from_encoder_decoder_models.py --output_dir vit_base_biomedlm --encoder_model_name_or_path google/vit-base-patch16-224-in21k --decoder_model_name_or_path stanford-crfm/BioMedLM
```


# Train/ Fine-tune the model on the data

To train the model on the data, run the following command:

`python3 run_captioning.py`

- `run_captioning.py` is a lightweight example of how to load and preprocess the rocov2 dataset, then fine-tune the vision-encoder-decoder architecture on it.
