import pandas as pd
import os
import datasets
from transformers import VisionEncoderDecoderModel, AutoFeatureExtractor,AutoTokenizer, ViTImageProcessor
from PIL import Image
from datasets import Dataset, DatasetDict
from transformers import DataCollatorForSeq2Seq

#os.environ["CUDA_VISIBLE_DEVICES"]="4"
os.environ["WANDB_PROJECT"]="vit-gpt"
os.environ["TOKENIZERS_PARALLELISM"]="true"

df_train = pd.read_csv("train_labels_caption_prediction.csv")
df_val = pd.read_csv("valid_labels_caption_prediction.csv")
df_test = pd.read_csv("test_labels_caption_prediction.csv")


df_train["ID"] = "roco2/train/" + df_train["ID"]+ ".jpg"
df_val["ID"] = "roco2/valid/" + df_val["ID"]+ ".jpg"
df_test["ID"] = "roco2/test/" + df_test["ID"]+ ".jpg"

# combine train and validation
df_train = df_train.append(df_val)

train_dataset = Dataset.from_pandas(df_train)
test_dataset = Dataset.from_pandas(df_test)

dataset_clef = DatasetDict({"train": train_dataset, "test": test_dataset})


model_name = "vit_base_biomedlm"

model = VisionEncoderDecoderModel.from_pretrained(model_name)
feature_extractor = ViTImageProcessor.from_pretrained(model_name)
tokenizer = AutoTokenizer.from_pretrained(model_name)
model.to("cuda:0")

# text preprocessing step
def tokenization_fn(captions, max_target_length):
    """Run tokenization on captions."""
    labels = tokenizer(captions, 
                        padding="max_length", 
                        max_length=max_target_length, truncation=True).input_ids

    return labels

# image preprocessing step
def feature_extraction_fn(image_paths, check_image=True):
    """
    Run feature extraction on images
    If `check_image` is `True`, the examples that fails during `Image.open()` will be caught and discarded.
    Otherwise, an exception will be thrown.
    """
    model_inputs = {}

    if check_image:
        images = []
        for image_path in image_paths:
            i_image = Image.open(image_path)
            if i_image.mode != "RGB":
                i_image = i_image.convert(mode="RGB")

            images.append(i_image)

    encoder_inputs = feature_extractor(images=images, return_tensors="pt")
    return encoder_inputs.pixel_values

def preprocess_fn(examples, max_target_length, check_image = True):
    """Run tokenization + image feature extraction"""
    image_paths = examples['ID']
    captions = examples['Caption']    
        
    model_inputs = {}
    # This contains image path column
    model_inputs['labels'] = tokenization_fn(captions, max_target_length)
    model_inputs['pixel_values'] = feature_extraction_fn(image_paths, check_image=check_image)

    return model_inputs

processed_dataset = dataset_clef.map(
        function=preprocess_fn,
        batched=True,
        fn_kwargs={"max_target_length": 128},
        num_proc=1
            )

from transformers import Seq2SeqTrainer, Seq2SeqTrainingArguments

training_args = Seq2SeqTrainingArguments(
        predict_with_generate=True,
        num_train_epochs=2,
        #eval_steps=1000,
        evaluation_strategy= "epoch",
        per_device_train_batch_size=2,
        per_device_eval_batch_size=2,
        gradient_accumulation_steps=2,
        output_dir="./image-captioning-output-vit-biomedlm-roco2",
        optim="adafactor",
        fp16=True,
        report_to="wandb"
            )
import evaluate
metric = evaluate.load("rouge")

import numpy as np
import nltk

try:
        nltk.data.find("tokenizers/punkt")
except (LookupError, OSError):
        nltk.download("punkt", quiet=True)

ignore_pad_token_for_loss = True

def postprocess_text(preds, labels):
    preds = [pred.strip() for pred in preds]
    labels = [label.strip() for label in labels]

    # rougeLSum expects newline after each sentence
    preds = ["\n".join(nltk.sent_tokenize(pred)) for pred in preds]
    labels = ["\n".join(nltk.sent_tokenize(label)) for label in labels]

    return preds, labels

def compute_metrics(eval_preds):
    preds, labels = eval_preds
    if isinstance(preds, tuple):
        preds = preds[0]
    decoded_preds = tokenizer.batch_decode(preds, skip_special_tokens=True)
    if ignore_pad_token_for_loss:
        # Replace -100 in the labels as we can't decode them.
        labels = np.where(labels != -100, labels, tokenizer.pad_token_id)
    decoded_labels = tokenizer.batch_decode(labels, skip_special_tokens=True)

    # Some simple post-processing
    decoded_preds, decoded_labels = postprocess_text(decoded_preds, decoded_labels)

    result = metric.compute(predictions=decoded_preds,  references=decoded_labels, use_stemmer=False)
    result = {k: round(v * 100, 4) for k, v in result.items()}
    prediction_lens = [ np.count_nonzero(pred != tokenizer.pad_token_id) for pred in preds]
    result["gen_len"] = np.mean(prediction_lens)
    return result


from transformers import default_data_collator

# instantiate trainer
trainer = Seq2SeqTrainer(
    model=model,
    tokenizer=feature_extractor,
    args=training_args,
    compute_metrics=compute_metrics,
    train_dataset=processed_dataset['train'],
    eval_dataset=processed_dataset['test'],
    data_collator=default_data_collator
        )

trainer.train()

trainer.save_model("./image-captioning-output-vit-biomedlm-roco2")
tokenizer.save_pretrained("./image-captioning-output-vit-biomedlm-roco2")
feature_extractor.save_pretrained("./image-captioning-output-vit-biomedlm-roco2")



